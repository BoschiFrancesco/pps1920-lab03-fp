package u03
import u02.Modules.Person.{Student, Teacher}
import u02.Optionals
import u02.Optionals._
import u02.Modules._
object Lists {

  // A generic linkedlist
  sealed trait List[E]

  // a companion object (i.e., module) for List
  object List {
    case class Cons[E](head: E, tail: List[E]) extends List[E]
    case class Nil[E]() extends List[E]

    def sum(l: List[Int]): Int = l match {
      case Cons(h, t) => h + sum(t)
      case _ => 0
    }

    def append[A](l1: List[A], l2: List[A]): List[A] = (l1, l2) match {
      case (Cons(h, t), l2) => Cons(h, append(t, l2))
      case _ => l2
    }

    def map[A,B](l: List[A])(mapper: A=>B): List[B] = l match {
      case Cons(h, t) => Cons(mapper(h), map(t)(mapper))
      case Nil() => Nil()
    }

    def filter[A](l1: List[A])(pred: A=>Boolean): List[A] = l1 match {
      case Cons(h,t) if (pred(h)) => Cons(h, filter(t)(pred))
      case Cons(_,t) => filter(t)(pred)
      case Nil() => Nil()
    }

    //todo cerca di fare kiss
    def drop[A](l: List[A], n: Int): List[A] = l match {
      case Cons(_,t) if (n == 0)=> l
      case Nil()=> Nil()
      case Cons(_,t) if(n > 0) => drop(t,n-1)
    }

    def flatMap[A,B](l:List[A])(f: A => List[B]) : List[B] = l match{
      case Nil() => Nil()
      case Cons(h,t) => append(f(h),flatMap(t)(f))
    }


    def mapInTermsOfFlatmap[A,B](l: List[A])(mapper: A=>B): List[B] = l match {
      case Cons(h, t) => flatMap(l)(v=> Cons(mapper(v), Nil()))
      case Nil() => Nil()
    }


    def filterInTermsOfFlatmap[A](l1: List[A])(pred: A=>Boolean): List[A] = l1 match {
      case Cons(h,t) => flatMap(l1)(v=> if(pred(v)) Cons(v,Nil()) else Nil())
      case Nil() => Nil()
    }


    def max(l: List[Int]): Option[Int] = {
      @annotation.tailrec
      def _fact(l: List[Int], acc: Option[Int]): Option[Int] = l match {
        case Cons(h,t) if Option.isEmpty(acc) || h > Option.getOrElse(acc,0) => _fact(t, Option.Some(h))
        case Cons(h,t) => _fact(t,acc)
        case Nil() => acc
      }
      _fact(l, Option.None())
    }

    def teachersCourses (l: List[Person]) : List[String] ={
      map(filter(l)(_.isInstanceOf[Teacher]))(_.asInstanceOf[Teacher].course)
    }
  }
}

object ListsMain extends App {
  import Lists._
  val l = List.Cons(10, List.Cons(20, List.Cons(30, List.Nil())))
  println(List.sum(l)) // 60
  import List._
  import u03.Lists.List
  println(append(Cons(5, Nil()), l)) // 5,10,20,30
  println(filter[Int](l)(_ >=20)) // 20,30

  val lst = Cons (10 , Cons (20 , Cons (30 , Nil () )) )
  println("lista es" + lst)
  println(drop ( lst ,1)) // Cons (20 , Cons (30 , Nil ()))
  println(drop ( lst ,2)) // Cons (30 , Nil ())
  println(drop ( lst ,5))

  val lst1 = Cons (10 , Cons (20 , Cons (30 , Nil () )) )
  println(flatMap ( lst1 )(v => Cons ( v +1 , Nil () )) ) // Cons (11 , Cons (21 , Cons (31 , Nil ())))
  println(flatMap ( lst1 )(v => Cons ( v +1 , Cons (v +2 , Nil () ))))

  println(max ( Cons (10 , Cons (25 , Cons (20 , Nil () ) ) ) ))
  println(max ( Nil () ))

  println(teachersCourses(List.Cons(Student("Mario", 1), List.Cons(Student("Mario", 3), List.Cons(Teacher("Maria", "Sgrodolo"), List.Cons(Teacher("Maria2", "Sgrodolina"), List.Nil()))))))

}